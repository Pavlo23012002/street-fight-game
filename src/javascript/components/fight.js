import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {


  return new Promise((resolve) => {

    let firstWinner = true;

    if(firstWinner) {
      resolve(firstFighter);
    } else{
      resolve(secondFighter);
    }
    // resolve the promise with the winner when fight is over
  });
}

export function getDamage(attacker, defender) {
  if (damage = getHitPower(attacker) - getBlockPower(defender) < 0){
    return 0;
  }
  return damage;
  // return damage
}

export function getHitPower(fighter) {
  criticalHitChance = Math.floor(Math.random() * 2) + 1;
  return fighter.attack * criticalHitChance;
  // return hit power
}

export function getBlockPower(fighter) {
  dodgeChance = Math.floor(Math.random()*2 +1);
  return fighter.defense * dodgeChance;
  // return block power
}
